﻿using System.Runtime.InteropServices;

internal class Program
{
    private static void Main(string[] args)
    {
        while (true) 
     {
        Console.WriteLine("Введите слово на русском (погода, дождь, облако, радуга, температура, давление, туман, закат, восход, закат, засуха): ");
        string[] weather = { "weather", "rain", "cloud", "rainbow", "temperature", "pressure", "fog", "sunrise", "dry", "sunset" };

        string word = Console.ReadLine();
        switch (word)
        {
            case "погода":
                Console.WriteLine(weather[0]);
                break;

            case "дождь":
                Console.WriteLine(weather[1]);
                break;

            case "облако":
                Console.WriteLine(weather[2]);
                break;

            case "радуга":
                Console.WriteLine(weather[3]);
                break;

            case "температура":
                Console.WriteLine(weather[4]);
                break;

            case "давление":
                Console.WriteLine(weather[5]);
                break;

            case "туман":
                Console.WriteLine(weather[6]);
                break;

            case "восход":
                Console.WriteLine(weather[7]);
                break;

            case "засуха":
                Console.WriteLine(weather[8]);
                break;

            case "закат":
                Console.WriteLine(weather[9]);
                break;

            default:
                Console.WriteLine("Данное слово на данный момент не известно программе");
                break;
        }
            Console.ReadLine();
     }
}
}